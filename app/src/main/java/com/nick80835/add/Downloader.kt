package com.nick80835.add

import android.content.SharedPreferences
import android.os.AsyncTask
import android.util.Log
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.File
import java.net.URL
import java.text.DecimalFormat

class Downloader : AsyncTask<URL, Int, Long>() {
    private val twoDF = DecimalFormat("0.00")
    private var currentProgress = 0
    var sharedPreferences: SharedPreferences? = null
    var downloadContainer: DownloadContainer? = null

    override fun doInBackground(vararg params: URL?): Long {
        aidService!!.newDownloadNotification(
            downloadContainer!!.taskId!!,
            downloadContainer!!.trackData!!.primaryName!!,
            downloadContainer!!.trackData!!.tertiaryName!!
        )

        try {
            val connection = params[0]!!.openConnection()
            connection.connect()

            val lengthOfFile = connection.contentLength

            val inputStream = connection.getInputStream()

            val outputFile = File(downloadContainer!!.filePathEncrypted!!)
            outputFile.createNewFile()

            var place = 0
            val chunk = ByteArray(16384)

            while (place < lengthOfFile) {
                val size = inputStream.read(chunk)
                val cutChunk = chunk.copyOfRange(0, size)

                place += size

                if (isCancelled) return 1
                publishProgress(place, lengthOfFile)

                if (sharedPreferences?.getBoolean("throttle_downloads", false) == true) {
                    Thread.sleep(50)
                }

                outputFile.appendBytes(cutChunk)
            }

            inputStream.close()
        } catch (e: Exception) {
            Log.e(TAG, e.message!!)
            return 2 // failed
        }

        Log.d(TAG, "Download succeeded: ${downloadContainer!!.taskId}")
        return 0 // succeeded
    }

    override fun onProgressUpdate(vararg values: Int?) {
        val progressPercent = (values[0]!! * 100) / values[1]!!

        if (progressPercent > currentProgress + 1 || progressPercent == 100) {
            currentProgress = progressPercent

            val progressSize = "${twoDF.format(values[0]!!.toFloat() / 1000000)}MB / ${twoDF.format(values[1]!!.toFloat() / 1000000)}MB"

            aidService!!.updateDownloadProgress(downloadContainer!!.taskId!!, currentProgress, progressSize)
        }
    }

    override fun onPostExecute(result: Long?) {
        if (result == 0.toLong()) {
            GlobalScope.launch {
                aidService!!.decryptDownloadedTrack(
                    downloadContainer!!.filePathEncrypted!!,
                    downloadContainer!!.blowfishKey!!,
                    downloadContainer!!.trackData!!,
                    downloadContainer!!.filePathComplete!!,
                    downloadContainer!!.taskId!!
                )
            }
        } else if (result == 2.toLong()) {
            Log.d(TAG, "Failing ${downloadContainer!!.taskId!!}")
            aidService!!.downloadEndNotification(downloadContainer!!.taskId!!, "Download failed")
            File(downloadContainer!!.filePathEncrypted!!).delete()
            aidService!!.removeTask(downloadContainer!!.taskId!!)
            aidService!!.killIfNeeded()
        }
    }

    override fun onCancelled() {
        super.onCancelled()

        Log.d(TAG, "Cancelling $downloadContainer!!.taskId!!")
        aidService!!.downloadCancelNotification(downloadContainer!!.taskId!!)
        File(downloadContainer!!.filePathEncrypted!!).delete()
        aidService!!.removeTask(downloadContainer!!.taskId!!)
        aidService!!.killIfNeeded()
    }
}
